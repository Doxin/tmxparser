import std.stdio;
import std.conv;
import dom=arsd.dom;
import file=std.file;
import std.exception:enforce;
import imageformats;
import path=std.path;

struct Tile
    {
    IFImage image;
    int gid;
    
    string toString()
        {
        return "Tile(<image>,"~this.gid.to!string()~")";
        }
    }

struct Tileset
    {
    string name;
    int firstgid;
    int tilewidth;
    int tileheight;
    Tile[] tiles;
    this(dom.Element element,string folder)
        {
        this.name=element.getAttribute("name");
        this.firstgid=to!int(element.getAttribute("firstgid"));
        this.tilewidth=to!int(element.getAttribute("tilewidth"));
        this.tileheight=to!int(element.getAttribute("tileheight"));
        foreach(dom.Element subelement; element.childNodes())
            {
            //writeln(subelement.tagName);
            switch(subelement.tagName)
                {
                case "#text":
                    break;
                case "image":
                    string fname=path.buildNormalizedPath([folder,subelement.getAttribute("source")]);
                    IFImage image=read_image(fname);
                    int tid=0;
                    for(int y=0;y<image.h;y+=this.tileheight)
                        for(int x=0;x<image.w;x+=this.tilewidth)
                            {
                            tid+=1;
                            this.tiles~=Tile(image,tid+this.firstgid);
                            }
                    break;
                default:
                    writeln("unhandled tile source: ",subelement.tagName);
                }
            }
        }
    }

struct Map
    {
    int width;
    int height;
    int tilewidth;
    int tileheight;
    Tileset[] tilesets;
    this(string filename)
        {
        writeln("parsing file");
        auto document=new dom.XmlDocument(to!string(file.read(filename)));
        dom.Element root=document.root;
        enforce(root.getAttribute("version")=="1.0","unsupported map format version "~root.getAttribute("version"));
        this.width=to!int(root.getAttribute("width"));
        this.height=to!int(root.getAttribute("height"));
        this.tilewidth=to!int(root.getAttribute("tilewidth"));
        this.tileheight=to!int(root.getAttribute("tileheight"));
        
        foreach(dom.Element tilesetelement; root.getElementsByTagName("tileset"))
            {
            this.tilesets~=Tileset(tilesetelement,path.dirName(filename));
            }
        }
    }

unittest
    {
    auto lvl=Map("testfiles/test.tmx");
    assert(lvl.width==10);
    assert(lvl.height==10);
    assert(lvl.tilewidth==16);
    assert(lvl.tileheight==16);
    //writeln(lvl);
    }

private IFImage crop(IFImage input,int x,int y,int w,int h)
    {
    int stride=input.c.to!int();
    IFImage output=IFImage(w,h,input.c,new ubyte[w*h*stride]);
    for(int rx=0;rx<w;rx+=1)
        for(int ry=0;ry<h;ry+=1)
            for(int stride_offset=0;stride_offset<stride;stride_offset+=1)
            {
            int target_offset=(rx+ry*w)*stride+stride_offset;
            int source_offset=((rx+x)+(ry+y)*w)*stride+stride_offset;
            output.pixels[target_offset]=input.pixels[source_offset];
            }
    return output;
    }

unittest
    {
    IFImage image=read_image("testfiles/tileset.png");
    auto newimage=image.crop(32,32,16,16);
    write_image("testfiles/cropresult.png",newimage.w,newimage.h,newimage.pixels);
    }
